#include <deque>
#include <algorithm>
#include <iostream>
//#include <boost/filesystem.hpp>
#include <fstream>
#include <stdint.h>
#include <memory>
#include <random>
#include <bitset>
#include <limits>
#include <assert.h>
#include <stdexcept>
#include <string.h>
#include <vector>
#include <time.h>
#include <math.h>


class LHCTriggers{
public:
  LHCTriggers();
  void Setup(unsigned long long raw_trigger_rate_khz,
	     unsigned long long collision_rate_khz
	     );
  void AddTriggerRule(int max_count, int period);
  void ClearTriggerRules();
  void SetSeed(unsigned seed);
  std::pair<int,bool> GetTriggerAndID();
  bool GetTrigger(){return GetTriggerAndID().second;}
private:
  std::vector<std::pair<int,int> > trigger_rules;
  unsigned long long raw_trigger_rate;
  unsigned long long collision_rate;
  std::vector<std::deque<int> > trigger_rule_counters;
  unsigned long long BX;
};


#include <boost/python.hpp> 
using namespace boost::python; 
 
BOOST_PYTHON_MODULE(LHCTriggers) 
{ 
  class_<LHCTriggers>("LHCTriggers") 
    .def("Setup", &LHCTriggers::Setup) 
    .def("AddTriggerRule", &LHCTriggers::AddTriggerRule) 
    .def("ClearTriggerRules", &LHCTriggers::ClearTriggerRules) 
    .def("SetSeed", &LHCTriggers::SetSeed)
    .def("GetTriggerAndID", &LHCTriggers::GetTriggerAndID)
    .def("GetTrigger", &LHCTriggers::GetTrigger) 
    ; 
}; 
