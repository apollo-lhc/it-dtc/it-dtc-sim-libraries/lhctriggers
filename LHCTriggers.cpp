#include "LHCTriggers.hh"

LHCTriggers::LHCTriggers(){
  srand((unsigned)time(NULL));  
}

void LHCTriggers::SetSeed(unsigned seed){
  srand(seed);
}

void LHCTriggers::Setup(unsigned long long raw_trigger_rate_khz = 750,
			unsigned long long collision_rate_khz = 40000){
  raw_trigger_rate = raw_trigger_rate_khz;
  collision_rate   = collision_rate_khz;

  ClearTriggerRules();
  
  BX = 0;
}

void LHCTriggers::AddTriggerRule(int max_count, int period){

  trigger_rules.push_back(std::make_pair(max_count,period));

  trigger_rule_counters.assign(trigger_rules.size(), std::deque<int>());

}

void LHCTriggers::ClearTriggerRules(){
  trigger_rules.clear();
  trigger_rule_counters.clear();
}

std::pair<int,bool> LHCTriggers::GetTriggerAndID(){
  bool trigger_condition = 1.0*rand()/double(RAND_MAX) < 1.0*double(raw_trigger_rate)/double(collision_rate);
  
  // Check the trigger rules
  for (int irule=0; irule<trigger_rules.size(); irule++) {

    if (trigger_rule_counters[irule].size()==0) {
      continue;
    }
    
    for (int ipast=0; ipast<trigger_rule_counters[irule].size(); ipast++){
      //track how many bx has past for each of the recent triggers, increment as time goes
      trigger_rule_counters[irule][ipast]++; 
    }

    if (trigger_rule_counters[irule].front()>=trigger_rules[irule].second){
      trigger_rule_counters[irule].pop_front();
      //triggers too long ago won't affect current trigger rule decision
    }
    trigger_condition = trigger_condition & (trigger_rule_counters[irule].size()<trigger_rules[irule].first);
  }    
  if (trigger_condition) {
    for (int irule=0; irule<trigger_rules.size(); irule++){
      trigger_rule_counters[irule].push_back(0);
    }
  }
  
  std::pair<int,bool> ret = {BX,trigger_condition};
  BX++;
  return ret;    
}
