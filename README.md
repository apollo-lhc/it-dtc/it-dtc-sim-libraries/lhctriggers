## LHCTriggers library
This library generates LHC triggers and trigger IDs to be use for simulation.
The code is written in C++ and has python bindings for use with cocotb.


## prereqs
boost-python3-devel

## Interface
The Setup call takes the collision rate and trigger rate in khz
Trigger rules can be added via AddTriggerRule(max trigger count, during #bxs)

The output is via GetTrigger() or GetTriggerAndID() and returns if there is a trigger during that bx and what the trigger ID would be.

## Usage
Please use git structure discussed [here](https://nvie.com/posts/a-successful-git-branching-model/).
