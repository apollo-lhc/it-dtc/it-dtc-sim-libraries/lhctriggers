# location of the Python header files
PYTHON_VERSION = 3.6m
#PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
# location of the Boost Python include files and library
#BOOST_INC = /usr/include
#BOOST_LIB = /usr/lib
# compile mesh classes
TARGET = LHCTriggers

$(TARGET) : $(TARGET).so
	ln -s $^ $@
$(TARGET).so: $(TARGET).o
	g++ -shared -Wl,--export-dynamic \
	-lboost_python3 -L/usr/lib/python$(PYTHON_VERSION)/config -lpython$(PYTHON_VERSION) \
	$^  -o $@
$(TARGET).o: $(TARGET).cpp
	g++ -I/usr/include/python3.6m -fPIC -c $^ -o $@ -std=c++11 

clean:
	rm -f $(TARGET).o
	rm -f $(TARGET).so
	rm -f $(TARGET)
